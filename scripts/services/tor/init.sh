#!/bin/bash

#DOCKER_BUILDKIT=0 docker build --network=mistborn_default -t mistborn_production_tor /opt/mistborn/compose/production/tor/

docker buildx create --use --name mistborn_tor_builder --driver-opt network=mistborn_default
docker buildx build -t mistborn_production_tor /opt/mistborn/compose/production/tor/